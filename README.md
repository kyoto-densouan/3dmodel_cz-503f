# README #

1/3スケールのSHARP X-1用FDDユニット風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-503f/raw/d5ef61d279e6b2e2b49cc884ef568f7dd232c093/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-503f/raw/d5ef61d279e6b2e2b49cc884ef568f7dd232c093/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-503f/raw/d5ef61d279e6b2e2b49cc884ef568f7dd232c093/ExampleImage.jpg)
